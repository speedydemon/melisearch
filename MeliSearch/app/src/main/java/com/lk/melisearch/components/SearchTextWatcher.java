package com.lk.melisearch.components;

import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;

public abstract class SearchTextWatcher implements TextWatcher {

    //To handle search with time semaphore
    protected static long TYPE_INTERVAL = 1000;

    private long mLastTypeTime = 0;

    /**
     * This method will be triggered every time that there is a pause of more than 1 second when typing.
     * @param searchText the resulting search text on the pause timer
     */
    protected abstract void onTypeEnd(String searchText);

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        long currentTypeTime = SystemClock.uptimeMillis();
        long elapsedTime = currentTypeTime - mLastTypeTime;

        if (elapsedTime <= TYPE_INTERVAL)
            return;

        mLastTypeTime = currentTypeTime;

        onTypeEnd(s.toString());
    }

}
