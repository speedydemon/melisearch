package com.lk.melisearch.activities.productSearch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Paging {

    @Expose
    public Integer total;
    @SerializedName("primary_results")
    @Expose
    public Integer primaryResults;
    @Expose
    public Integer offset;
    @Expose
    public Integer limit;

}
