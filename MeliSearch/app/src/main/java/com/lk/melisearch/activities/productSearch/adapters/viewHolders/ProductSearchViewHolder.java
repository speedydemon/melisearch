package com.lk.melisearch.activities.productSearch.adapters.viewHolders;

import android.view.View;
import android.widget.TextView;

import com.lk.melisearch.R;
import com.lk.melisearch.activities.productSearch.models.Product;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.lk.melisearch.widgets.AmountView;
import com.squareup.picasso.Picasso;

public class ProductSearchViewHolder extends RecyclerView.ViewHolder {

    private final AppCompatImageView mProductImage;
    private final TextView mProductDescription;
    private final AmountView mProductPrice;

    public ProductSearchViewHolder(@NonNull View itemView) {
        super(itemView);

        mProductImage = itemView.findViewById(R.id.product_image);
        mProductDescription = itemView.findViewById(R.id.product_description);
        mProductPrice = itemView.findViewById(R.id.product_amount);
    }

    public void bind(Product product) {
        Picasso.get().load(product.getThumbnailUrl()).into(mProductImage);
        mProductDescription.setText(product.getTitle());
        mProductPrice.setAmount(product.getPrice());
    }

}
