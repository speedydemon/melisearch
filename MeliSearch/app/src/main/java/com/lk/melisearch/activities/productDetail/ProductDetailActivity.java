package com.lk.melisearch.activities.productDetail;

import android.content.Intent;
import android.os.Bundle;

import com.lk.melisearch.R;
import com.lk.melisearch.activities.AbstractFragmentActivity;
import com.lk.melisearch.activities.productDetail.fragments.ProductDetailFragment;
import com.lk.melisearch.activities.productSearch.ProductSearchActivity;

import androidx.fragment.app.Fragment;

public class ProductDetailActivity extends AbstractFragmentActivity {

    public static final String PRODUCT_DETAIL_ID = "PRODUCT_DETAIL_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        Intent intent = getIntent();
        String productId = intent.getExtras().getString(ProductSearchActivity.INTENT_PRODUCT_KEY);

        Fragment startFragment = new ProductDetailFragment();
        Bundle args = new Bundle();
        args.putString(PRODUCT_DETAIL_ID, productId);
        startFragment.setArguments(args);

        replaceFragment(startFragment);
    }

}