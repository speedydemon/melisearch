package com.lk.melisearch.activities;

import android.os.Bundle;
import android.view.WindowManager;

import com.lk.melisearch.R;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public abstract class AbstractFragmentActivity extends AppCompatActivity {

    public final transient String TAG = getClass().getSimpleName();

    /*
     ***************** Lifecycle *****************
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayout());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    /*
     ***************** Listeners *****************
     */

    /*
     ***************** Methods *****************
     */

    protected void replaceFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        transaction.setCustomAnimations(
            R.anim.enter_from_right, R.anim.exit_to_left,
            R.anim.enter_from_left, R.anim.exit_to_right
        );
        transaction.replace(getContainer(), fragment);

        transaction.commit();
    }

    @IdRes
    protected int getContainer() {
        return R.id.activity_fragment;
    }

    @LayoutRes
    protected int getLayout() {
        return R.layout.activity_fragment;
    }

}
