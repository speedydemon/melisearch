package com.lk.melisearch.activities.productSearch.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.lk.melisearch.R;
import com.lk.melisearch.activities.AbstractFragment;
import com.lk.melisearch.activities.productSearch.adapters.ProductSearchAdapter;
import com.lk.melisearch.activities.productSearch.callbacks.DataCallback;
import com.lk.melisearch.activities.productSearch.models.Product;
import com.lk.melisearch.activities.productSearch.services.ProductService;
import com.lk.melisearch.dialogs.LoaderDialog;

import java.net.HttpURLConnection;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class ProductSearchFragment extends AbstractFragment {

    private static final String SEARCHED = "SEARCHED";
    private static final String SEARCH_TEXT = "SEARCH_TEXT";

    public interface ProductSearchFragmentListener {
        void onProductDetail(Product product);
    }

    private EditText mSearchView;
    private RecyclerView mProductListView;
    private TextView mSearchError;

    private ProductSearchAdapter mProductListAdapter;

    private ProductSearchFragmentListener mListener;

    private boolean mSearched = false;

    /*
     ***************** Lifecycle *****************
     */

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        mListener = (ProductSearchFragmentListener) context;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_product_search;
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        mSearchView = view.findViewById(R.id.product_search_input);
        mSearchError = view.findViewById(R.id.product_search_error);

        mProductListView = view.findViewById(R.id.product_search_results);
        mProductListView.setHasFixedSize(false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        mSearchView.addTextChangedListener(new SearchTextWatcher() {
//            @Override
//            protected void onTypeEnd(String searchText) {
//                //TODO autocomplete suggestions
//                Log.d(TAG, "Typing: " + searchText);
//            }
//        });

        mSearchView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                updateProductList();
                return true;
            }
            return false;
        });
    }

    /*
     ***************** Listeners *****************
     */

    /*
     ***************** Methods *****************
     */

    private void updateProductList() {
        mSearched = true;

        LoaderDialog.show(getParentFragmentManager());
        ProductService.getInstance().searchProducts(mSearchView.getText().toString(), new DataCallback<List<Product>>() {
            @Override
            public void onSuccess(List<Product> productList) {
                mSearchError.setVisibility(View.GONE);
                if (mProductListAdapter == null) {
                    mProductListAdapter = new ProductSearchAdapter(requireContext(), productList, mListener);
                    mProductListView.setAdapter(mProductListAdapter);
                }
                else {
                    mProductListAdapter.clear();
                    mProductListAdapter.addAllItems(productList);
                }
                LoaderDialog.hide();
            }

            @Override
            public void onFailure(int code) {
                mSearchError.setVisibility(View.VISIBLE);
                if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                    mSearchError.setText(R.string.product_search_error_not_found);
                    return;
                }

                mSearchError.setText(R.string.product_search_error_unexpected);
                LoaderDialog.hide();
            }
        });
    }

}
