package com.lk.melisearch.activities.productDetail.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.lk.melisearch.R;
import com.lk.melisearch.activities.AbstractFragment;
import com.lk.melisearch.activities.productDetail.ProductDetailActivity;
import com.lk.melisearch.activities.productDetail.adapters.ProductDetailAdapter;
import com.lk.melisearch.activities.productDetail.adapters.ProductDetailImagePagerAdapter;
import com.lk.melisearch.activities.productSearch.ProductSearchActivity;
import com.lk.melisearch.activities.productSearch.callbacks.DataCallback;
import com.lk.melisearch.activities.productSearch.models.Product;
import com.lk.melisearch.activities.productSearch.services.ProductService;
import com.lk.melisearch.dialogs.LoaderDialog;
import com.lk.melisearch.widgets.AmountView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import hyogeun.github.com.colorratingbarlib.ColorRatingBar;

public class ProductDetailFragment extends AbstractFragment {

    private String mProductId;

    private TextView mQualitySold;
    private TextView mTitle;
    private ColorRatingBar mRating;
    private TabLayout mImageTab;
    private ViewPager2 mImagePager;
    private AmountView mAmount;
    private RecyclerView mDetails;

    private ProductDetailAdapter mDetailsAdapter;

    /*
     ***************** Lifecycle *****************
     */

    @Override
    protected int getLayout() {
        return R.layout.fragment_product_detail;
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        mProductId = getArguments().getString(ProductDetailActivity.PRODUCT_DETAIL_ID, "");

        mQualitySold = view.findViewById(R.id.product_quality_sold);
        mTitle = view.findViewById(R.id.product_description);
        mRating = view.findViewById(R.id.product_rating);

        mImageTab = view.findViewById(R.id.product_image_tab);
        mImagePager = view.findViewById(R.id.product_image_pager);

        mAmount = view.findViewById(R.id.product_amount);

        mDetails = view.findViewById(R.id.product_details);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        searchProduct();
    }

    /*
     ***************** Listeners *****************
     */

    /*
     ***************** Methods *****************
     */

    private void searchProduct() {
        LoaderDialog.show(getParentFragmentManager());
        ProductService.getInstance().searchProduct(mProductId, new DataCallback<Product>() {
            @Override
            public void onSuccess(Product product) {
                setupProductDetail(product);
            }

            @Override
            public void onFailure(int code) {
                Toast.makeText(requireContext(), R.string.product_detail_error_unexpected, Toast.LENGTH_LONG).show();
                LoaderDialog.hide();
            }
        });
    }

    private void setupProductDetail(Product product) {
        mQualitySold.setText(getString(
            R.string.product_detail_quality_sold,
            getString(product.getQualityId()),
            product.getSold()
        ));

        mTitle.setText(product.getTitle());

//        mRating.setRating(product.); //TODO set corresponding rating to product

        mImagePager.setAdapter(new ProductDetailImagePagerAdapter(getActivity(), product.getPictures()));
        new TabLayoutMediator(mImageTab, mImagePager, (tab, position) -> {//just need to connect
        }).attach();

        mAmount.setAmount(product.getPrice());

        if (mDetailsAdapter == null) {
            mDetailsAdapter = new ProductDetailAdapter(requireContext(), product.getAttributes());
            mDetails.setAdapter(mDetailsAdapter);
        }
        else {
            mDetailsAdapter.clear();
            mDetailsAdapter.addAllItems(product.getAttributes());
        }

        LoaderDialog.hide();
    }

}
