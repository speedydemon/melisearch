package com.lk.melisearch.activities.productSearch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lk.melisearch.R;
import com.lk.melisearch.activities.productDetail.models.ProductAttribute;
import com.lk.melisearch.activities.productDetail.models.ProductImage;

import java.util.List;

public class Product {

    @Expose
    public String id;
    @SerializedName("site_id")
    @Expose
    public String siteId;
    @Expose
    public String title;
    @Expose
    public Object seller;
    @Expose
    public Double price;
    @Expose
    public Object prices;
    @SerializedName("sale_price")
    @Expose
    public Object salePrice;
    @SerializedName("currency_id")
    @Expose
    public String currencyId;
    @SerializedName("available_quantity")
    @Expose
    public Integer availableQuantity;
    @SerializedName("sold_quantity")
    @Expose
    public Integer soldQuantity;
    @SerializedName("buying_mode")
    @Expose
    public String buyingMode;
    @SerializedName("listing_type_id")
    @Expose
    public String listingTypeId;
    @SerializedName("stop_time")
    @Expose
    public String stopTime;
    @Expose
    public String condition;
    @Expose
    public String permalink;
    @Expose
    public String thumbnail;
    @SerializedName("thumbnail_id")
    @Expose
    public String thumbnailId;
    @Expose
    public List<ProductImage> pictures;
    @SerializedName("accepts_mercadopago")
    @Expose
    public boolean acceptsMercadopago;
    @Expose
    public Object installments;
    @Expose
    public Object address;
    @Expose
    public Object shipping;
    @SerializedName("seller_address")
    @Expose
    public Object sellerAddress;
    @Expose
    public List<ProductAttribute> attributes;
    @SerializedName("original_price")
    @Expose
    public Object originalPrice;
    @SerializedName("category_id")
    @Expose
    public String categoryId;
    @SerializedName("official_store_id")
    @Expose
    public Object officialStoreId;
    @SerializedName("domain_id")
    @Expose
    public String domainId;
    @SerializedName("catalog_product_id")
    @Expose
    public String catalogProductId;
    @Expose
    public List<Object> tags = null;
    @SerializedName("catalog_listing")
    @Expose
    public boolean catalogListing;
    @SerializedName("use_thumbnail_id")
    @Expose
    public boolean useThumbnailId;
    @SerializedName("order_backend")
    @Expose
    public Integer orderBackend;

    public String getTitle() {
        return title;
    }

    public Double getPrice() {
        return price;
    }

    public String getThumbnailUrl() {
        if (thumbnail.startsWith("http:")) {
            return thumbnail.replace("http:", "https:");
        }
        return thumbnail;
    }

    public String getId() {
        return id;
    }

    public Integer getSold() {
        return soldQuantity;
    }

    public int getQualityId() {
        if (condition.equals("new")) return R.string.product_detail_new;
        return R.string.product_detail_used;
    }

    public List<ProductImage> getPictures() {
        return pictures;
    }

    public List<ProductAttribute> getAttributes() {
        return attributes;
    }
}
