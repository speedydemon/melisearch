package com.lk.melisearch.activities.productSearch.callbacks;

public interface DataCallback<T> {

	void onSuccess(T response);

	void onFailure(int code);

}
