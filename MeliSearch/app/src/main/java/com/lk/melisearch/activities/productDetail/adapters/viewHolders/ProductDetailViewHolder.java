package com.lk.melisearch.activities.productDetail.adapters.viewHolders;

import android.view.View;
import android.widget.TextView;

import com.lk.melisearch.R;
import com.lk.melisearch.activities.productDetail.models.ProductAttribute;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProductDetailViewHolder extends RecyclerView.ViewHolder {

    private final TextView mDetailTitle;
    private final TextView mDetailDescription;

    public ProductDetailViewHolder(@NonNull View itemView) {
        super(itemView);

        mDetailTitle = itemView.findViewById(R.id.detail_name);
        mDetailDescription = itemView.findViewById(R.id.detail_description);
    }

    public void bind(ProductAttribute attribute) {
        mDetailTitle.setText(attribute.getName());
        mDetailDescription.setText(attribute.getValueName());
    }

}
