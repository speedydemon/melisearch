package com.lk.melisearch.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class AbstractFragment extends Fragment implements View.OnClickListener {

    public final String TAG = getClass().getSimpleName();

    private Handler handler;

    /*
     ***************** Lifecycle *****************
     */

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayout(), container, false);
    }

    /*
     ***************** Methods *****************
     */

    @LayoutRes
    protected abstract int getLayout();

    protected void runInBackground(Runnable pendingRunnable) {
        if (pendingRunnable != null) {
            new Thread(pendingRunnable).start();
        }
    }

    protected void runInUiThread(Runnable pendingRunnable) {
        if (pendingRunnable != null) {
            getHandler().post(pendingRunnable);
        }
    }

    protected void runInUiThread(Runnable pendingRunnable, long millis) {
        if (pendingRunnable != null) {
            getHandler().postDelayed(pendingRunnable, millis);
        }
    }

    public Handler getHandler() {
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        return handler;
    }

    protected void showSoftKeyboard(View view) {
        if (view == null) {
            Log.d(TAG, "Try to request focus on NULL view");
            return;
        }

        InputMethodManager manager = (InputMethodManager) requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        if (manager != null) {
            manager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
        view.requestFocus();
    }

    protected void hideSoftKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view == null) {
            Log.d(TAG, "Try to remove focus on NULL view");
            return;
        }

        InputMethodManager manager = (InputMethodManager) requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE);

        if (manager != null) {
            manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //To handle click with time semaphore
    protected static long CLICK_INTERVAL = 500;

    private long mLastClickTime = 0;

    protected void onSingleClick(View v) {
        /**
         * Remember to always reset the button to enabled after you are done with it
         */
    }

    @Override
    public void onClick(View v) {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;

        if (elapsedTime <= CLICK_INTERVAL)
            return;

        mLastClickTime = currentClickTime;
        v.setEnabled(false);

        onSingleClick(v);
    }

}
