package com.lk.melisearch.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lk.melisearch.R;

import androidx.annotation.Nullable;

public class AmountView extends RelativeLayout implements TextView.OnEditorActionListener {

    public interface AmountChangeListener {
        void onAmountChanged(Double amount);

        void onDone();
    }

    private final static String defaultAmount = "0";

    private TextView currencyText;
    private EditText amountText;

    private Double maximum = -1d;
    private Double minimum = -1d;

    private boolean mutable = false;

    private AmountChangeListener listener;

    public AmountView(Context context) {
        this(context, null);
    }

    public AmountView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AmountView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrSet) {
        if (attrSet == null) {
            return;
        }

        TypedArray array = context.obtainStyledAttributes(attrSet, R.styleable.AmountView);

        try {
            mutable = array.getBoolean(R.styleable.AmountView_mutable, false);
        }
        finally {
            array.recycle();
        }

        RelativeLayout view = (RelativeLayout) LayoutInflater.from(getContext()).inflate(R.layout.widget_amount, null, false);

        amountText = view.findViewById(R.id.widget_amount);
        currencyText = view.findViewById(R.id.widget_currency);

        amountText.setText(defaultAmount);
        amountText.setEnabled(mutable);

        addView(view);

        amountText.setClickable(mutable);
        amountText.setFocusable(mutable);
        currencyText.setClickable(mutable);
        currencyText.setFocusable(mutable);

        if (!mutable) {
            amountText.setInputType(EditorInfo.TYPE_NULL);
        }

        this.setClickable(!mutable);
        this.setFocusable(!mutable);

        if (mutable) {
            setupAmountBehaviour();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setupAmountBehaviour() {
        amountText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                onAmountChanged();
            }

        });

        amountText.setOnEditorActionListener(this);
    }

    public Double getAmount() {
        return Double.valueOf(amountText.getText().toString());
    }

    public String getAmountString() {
        return amountText.getText().toString();
    }

    public void setAmount(Double amount) {
        setAmount(amount.toString());
    }

    public void setAmount(String amount) {
        amountText.setText(amount);
    }

    /**
     * Limiters
     */

    public void setMinimum(Double minimum) {
        this.minimum = minimum;
    }

    public void setMaximum(Double maximum) {
        this.maximum = maximum;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        amountText.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                l.onClick(v);
            }
            return true;
        });
    }

    /**
     * Listener Actions
     */

    public void setListener(AmountChangeListener listener) {
        this.listener = listener;
    }

    private void onAmountChanged() {
        if (minimum != -1d && getAmount() < minimum) {
            setAmount(minimum);
            return;
        }

        if (maximum != -1d && getAmount() > maximum) {
            setAmount(maximum);
            return;
        }

        if (listener != null) {
            listener.onAmountChanged(getAmount());
        }
    }

    private void onDone() {
        if (listener != null) {
            listener.onDone();
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onDone();
            return true;
        }
        return false;
    }

}
