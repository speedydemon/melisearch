package com.lk.melisearch.activities.productDetail.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductImage {

    @Expose
    private String id;
    @Expose
    private String url;
    @SerializedName("secure_url")
    @Expose
    private String secureUrl;
    @Expose
    private String size;
    @SerializedName("max_size")
    @Expose
    private String maxSize;
    @Expose
    private String quality;

    public String getSecureUrl() {
        return secureUrl;
    }

}
