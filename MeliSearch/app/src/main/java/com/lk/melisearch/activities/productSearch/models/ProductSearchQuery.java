package com.lk.melisearch.activities.productSearch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductSearchQuery {

    @SerializedName("site_id")
    @Expose
    public String siteId;
    @Expose
    public String query;
    @Expose
    public Paging paging;
    @SerializedName("results")
    @Expose
    public List<Product> products;

    //gonna ignore other fields for now https://api.mercadolibre.com/sites/MLA/search?q=Motorola

}
