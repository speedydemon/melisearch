package com.lk.melisearch.activities.productSearch.services;

import com.lk.melisearch.activities.productSearch.models.Product;
import com.lk.melisearch.activities.productSearch.models.ProductSearchQuery;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ProductApi {

    @GET("sites/MLA/search/")
    Call<ProductSearchQuery> searchProducts(@Query("q") String searchText);

    @GET("items/{productId}")
    Call<Product> searchProduct(@Path("productId") String productId);

}
