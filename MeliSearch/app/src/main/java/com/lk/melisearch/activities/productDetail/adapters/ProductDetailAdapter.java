package com.lk.melisearch.activities.productDetail.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.lk.melisearch.R;
import com.lk.melisearch.activities.AbstractRecyclerViewAdapter;
import com.lk.melisearch.activities.productDetail.adapters.viewHolders.ProductDetailViewHolder;
import com.lk.melisearch.activities.productDetail.models.ProductAttribute;

import java.util.List;

import androidx.annotation.NonNull;

public class ProductDetailAdapter extends AbstractRecyclerViewAdapter<ProductAttribute, ProductDetailViewHolder> {

    public ProductDetailAdapter(Context context, List<ProductAttribute> attributes) {
        super(context, attributes);
    }

    @NonNull
    @Override
    public ProductDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductDetailViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_product_detail, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductDetailViewHolder holder, int position) {
        holder.bind(getItem(position));
        holder.itemView.setActivated(position % 2 == 0);
    }

}
