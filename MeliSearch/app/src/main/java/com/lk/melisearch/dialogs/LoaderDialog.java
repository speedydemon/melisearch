package com.lk.melisearch.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.lk.melisearch.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

public class LoaderDialog extends DialogFragment {

    private static LoaderDialog instance;
    public static String TAG = "LoaderDialog";

    private CharSequence title;

    public LoaderDialog() {
        this(null);
    }

    public LoaderDialog(CharSequence title) {
        this.title = title;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.LoaderDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_loader, null);

        setCancelable(false);

        if (title != null) {
            ((TextView) view.findViewById(R.id.loader_dialog_title)).setText(title);
        }
        return view;
    }

    @Override
    public void onResume() {
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setBackgroundDrawableResource(R.color.white);

        super.onResume();
    }

    protected void updateTitle(CharSequence message) {
        synchronized (LoaderDialog.this) {
            View view = getView();

            title = message != null ? message : "";

            if (view != null) {
                ((TextView) view.findViewById(R.id.loader_dialog_title)).setText(title);
            }
        }
    }

    public static LoaderDialog getInstance() {
        return instance;
    }

    public static LoaderDialog show(FragmentManager manager) {
        return show(null, manager);
    }

    public static LoaderDialog show(CharSequence message, FragmentManager manager) {
        synchronized (LoaderDialog.class) {
            if (instance != null) {
                return update(message, manager);
            }

            instance = new LoaderDialog(message);
            instance.show(manager, TAG);
            return instance;
        }
    }

    public static LoaderDialog update(CharSequence message, FragmentManager manager) {
        synchronized (LoaderDialog.class) {
            if (instance == null) {
                return show(message, manager);
            }

            instance.updateTitle(message);
            return instance;
        }
    }

    public static void hide() {
        synchronized (LoaderDialog.class) {
            if (instance != null) {
                try {
                    instance.dismiss();
                }
                catch (Exception e) {
                    // Ignore
                }
                instance = null;
            }
        }
    }

}
