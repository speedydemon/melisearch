package com.lk.melisearch.activities;

import android.os.Build;
import android.util.Log;

import com.lk.melisearch.BuildConfig;
import com.lk.melisearch.app.Constants;

import org.conscrypt.Conscrypt;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

import androidx.annotation.NonNull;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class AbstractService<T> {

	protected final String TAG = getClass().getSimpleName();

	protected T service;

	protected AbstractService(String baseUrl) {
		OkHttpClient.Builder builder = new OkHttpClient.Builder();

		builder.callTimeout(Constants.APITimeout, TimeUnit.SECONDS);
		builder.connectTimeout(Constants.APITimeout, TimeUnit.SECONDS);
		builder.readTimeout(Constants.APITimeout, TimeUnit.SECONDS);
		builder.writeTimeout(Constants.APITimeout, TimeUnit.SECONDS);

		if (BuildConfig.DEBUG) {
			HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
			interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
			builder.addInterceptor(interceptor);
		}

		// Fixed Bug :: SSL handshake aborted on Android 4.x
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
			try {
				Security.insertProviderAt(Conscrypt.newProvider(), 1);
				SSLContext context = SSLContext.getInstance("TLSv1");
				context.init(null, null, null);
				builder.sslSocketFactory(new NoSSLv3SocketFactory(context.getSocketFactory()), new NoX509TrustManager());
			} catch (Exception exc) {
				Log.e(TAG, "Error while setting TLSv1", exc);
			}
		}

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(baseUrl)
				.client(builder.build())
				.addConverterFactory(GsonConverterFactory.create(Constants.gsonApi))
				.build();

		service = retrofit.create(getServiceClass());
	}

	protected abstract Class<T> getServiceClass();

	private final static class NoX509TrustManager implements X509TrustManager {

		@Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) {
		}

		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) {
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return new X509Certificate[0];
		}

	}

	public static class NoSSLv3SocketFactory extends SSLSocketFactory {

		private final SSLSocketFactory delegate;

		NoSSLv3SocketFactory(SSLSocketFactory delegate) {
			this.delegate = delegate;
		}

		@Override
		public String[] getDefaultCipherSuites() {
			return delegate.getDefaultCipherSuites();
		}

		@Override
		public String[] getSupportedCipherSuites() {
			return delegate.getSupportedCipherSuites();
		}

		private Socket makeSocketSafe(Socket socket) {
			if (socket instanceof SSLSocket) {
				socket = new NoSSLv3SSLSocket((SSLSocket) socket);
			}
			return socket;
		}

		@Override
		public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
			return makeSocketSafe(delegate.createSocket(s, host, port, autoClose));
		}

		@Override
		public Socket createSocket(String host, int port) throws IOException {
			return makeSocketSafe(delegate.createSocket(host, port));
		}

		@Override
		public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException {
			return makeSocketSafe(delegate.createSocket(host, port, localHost, localPort));
		}

		@Override
		public Socket createSocket(InetAddress host, int port) throws IOException {
			return makeSocketSafe(delegate.createSocket(host, port));
		}

		@Override
		public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
			return makeSocketSafe(delegate.createSocket(address, port, localAddress, localPort));
		}

		private final static class NoSSLv3SSLSocket extends DelegateSSLSocket {

			private NoSSLv3SSLSocket(SSLSocket delegate) {
				super(delegate);
			}

			@Override
			public void setEnabledProtocols(String[] protocols) {
				if (protocols != null && protocols.length == 1 && "SSLv3".equals(protocols[0])) {
					List<String> enabledProtocols = new ArrayList<>(Arrays.asList(delegate.getEnabledProtocols()));

					if (enabledProtocols.size() > 1) {
						enabledProtocols.remove("SSLv3");
					}
					protocols = enabledProtocols.toArray(new String[enabledProtocols.size()]);
				}

				super.setEnabledProtocols(protocols);
			}
		}

		private static class DelegateSSLSocket extends SSLSocket {

			final SSLSocket delegate;

			DelegateSSLSocket(SSLSocket delegate) {
				this.delegate = delegate;
			}

			@Override
			public String[] getSupportedCipherSuites() {
				return delegate.getSupportedCipherSuites();
			}

			@Override
			public String[] getEnabledCipherSuites() {
				return delegate.getEnabledCipherSuites();
			}

			@Override
			public void setEnabledCipherSuites(String[] suites) {
				delegate.setEnabledCipherSuites(suites);
			}

			@Override
			public String[] getSupportedProtocols() {
				return delegate.getSupportedProtocols();
			}

			@Override
			public String[] getEnabledProtocols() {
				return delegate.getEnabledProtocols();
			}

			@Override
			public void setEnabledProtocols(String[] protocols) {
				delegate.setEnabledProtocols(protocols);
			}

			@Override
			public SSLSession getSession() {
				return delegate.getSession();
			}

			@Override
			public void addHandshakeCompletedListener(HandshakeCompletedListener listener) {
				delegate.addHandshakeCompletedListener(listener);
			}

			@Override
			public void removeHandshakeCompletedListener(HandshakeCompletedListener listener) {
				delegate.removeHandshakeCompletedListener(listener);
			}

			@Override
			public void startHandshake() throws IOException {
				delegate.startHandshake();
			}

			@Override
			public void setUseClientMode(boolean mode) {
				delegate.setUseClientMode(mode);
			}

			@Override
			public boolean getUseClientMode() {
				return delegate.getUseClientMode();
			}

			@Override
			public void setNeedClientAuth(boolean need) {
				delegate.setNeedClientAuth(need);
			}

			@Override
			public void setWantClientAuth(boolean want) {
				delegate.setWantClientAuth(want);
			}

			@Override
			public boolean getNeedClientAuth() {
				return delegate.getNeedClientAuth();
			}

			@Override
			public boolean getWantClientAuth() {
				return delegate.getWantClientAuth();
			}

			@Override
			public void setEnableSessionCreation(boolean flag) {
				delegate.setEnableSessionCreation(flag);
			}

			@Override
			public boolean getEnableSessionCreation() {
				return delegate.getEnableSessionCreation();
			}

			@Override
			public void bind(SocketAddress localAddr) throws IOException {
				delegate.bind(localAddr);
			}

			@Override
			public synchronized void close() throws IOException {
				delegate.close();
			}

			@Override
			public void connect(SocketAddress remoteAddr) throws IOException {
				delegate.connect(remoteAddr);
			}

			@Override
			public void connect(SocketAddress remoteAddr, int timeout) throws IOException {
				delegate.connect(remoteAddr, timeout);
			}

			@Override
			public SocketChannel getChannel() {
				return delegate.getChannel();
			}

			@Override
			public InetAddress getInetAddress() {
				return delegate.getInetAddress();
			}

			@Override
			public InputStream getInputStream() throws IOException {
				return delegate.getInputStream();
			}

			@Override
			public boolean getKeepAlive() throws SocketException {
				return delegate.getKeepAlive();
			}

			@Override
			public InetAddress getLocalAddress() {
				return delegate.getLocalAddress();
			}

			@Override
			public int getLocalPort() {
				return delegate.getLocalPort();
			}

			@Override
			public SocketAddress getLocalSocketAddress() {
				return delegate.getLocalSocketAddress();
			}

			@Override
			public boolean getOOBInline() throws SocketException {
				return delegate.getOOBInline();
			}

			@Override
			public OutputStream getOutputStream() throws IOException {
				return delegate.getOutputStream();
			}

			@Override
			public int getPort() {
				return delegate.getPort();
			}

			@Override
			public synchronized int getReceiveBufferSize() throws SocketException {
				return delegate.getReceiveBufferSize();
			}

			@Override
			public SocketAddress getRemoteSocketAddress() {
				return delegate.getRemoteSocketAddress();
			}

			@Override
			public boolean getReuseAddress() throws SocketException {
				return delegate.getReuseAddress();
			}

			@Override
			public synchronized int getSendBufferSize() throws SocketException {
				return delegate.getSendBufferSize();
			}

			@Override
			public int getSoLinger() throws SocketException {
				return delegate.getSoLinger();
			}

			@Override
			public synchronized int getSoTimeout() throws SocketException {
				return delegate.getSoTimeout();
			}

			@Override
			public boolean getTcpNoDelay() throws SocketException {
				return delegate.getTcpNoDelay();
			}

			@Override
			public int getTrafficClass() throws SocketException {
				return delegate.getTrafficClass();
			}

			@Override
			public boolean isBound() {
				return delegate.isBound();
			}

			@Override
			public boolean isClosed() {
				return delegate.isClosed();
			}

			@Override
			public boolean isConnected() {
				return delegate.isConnected();
			}

			@Override
			public boolean isInputShutdown() {
				return delegate.isInputShutdown();
			}

			@Override
			public boolean isOutputShutdown() {
				return delegate.isOutputShutdown();
			}

			@Override
			public void sendUrgentData(int value) throws IOException {
				delegate.sendUrgentData(value);
			}

			@Override
			public void setKeepAlive(boolean keepAlive) throws SocketException {
				delegate.setKeepAlive(keepAlive);
			}

			@Override
			public void setOOBInline(boolean oobinline) throws SocketException {
				delegate.setOOBInline(oobinline);
			}

			@Override
			public void setPerformancePreferences(int connectionTime, int latency, int bandwidth) {
				delegate.setPerformancePreferences(connectionTime, latency, bandwidth);
			}

			@Override
			public synchronized void setReceiveBufferSize(int size) throws SocketException {
				delegate.setReceiveBufferSize(size);
			}

			@Override
			public void setReuseAddress(boolean reuse) throws SocketException {
				delegate.setReuseAddress(reuse);
			}

			@Override
			public synchronized void setSendBufferSize(int size) throws SocketException {
				delegate.setSendBufferSize(size);
			}

			@Override
			public void setSoLinger(boolean on, int timeout) throws SocketException {
				delegate.setSoLinger(on, timeout);
			}

			@Override
			public synchronized void setSoTimeout(int timeout) throws SocketException {
				delegate.setSoTimeout(timeout);
			}

			@Override
			public void setTcpNoDelay(boolean on) throws SocketException {
				delegate.setTcpNoDelay(on);
			}

			@Override
			public void setTrafficClass(int value) throws SocketException {
				delegate.setTrafficClass(value);
			}

			@Override
			public void shutdownInput() throws IOException {
				delegate.shutdownInput();
			}

			@Override
			public void shutdownOutput() throws IOException {
				delegate.shutdownOutput();
			}

			@Override
			@NonNull
			public String toString() {
				return delegate.toString();
			}

			@Override
			public boolean equals(Object o) {
				return delegate.equals(o);
			}
		}
	}

}
