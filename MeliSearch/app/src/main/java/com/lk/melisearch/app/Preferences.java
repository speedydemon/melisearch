package com.lk.melisearch.app;

import android.content.Context;
import android.content.SharedPreferences;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.Nullable;

public class Preferences {

    // Private mode
    private final static int DEFAULT_MODE = Context.MODE_PRIVATE;

    private final SharedPreferences sharedPreferences;

    public Preferences(Context context, String name) {
        this(context, name, DEFAULT_MODE);
    }

    public Preferences(Context context, String name, int mode) {
        sharedPreferences = context.getSharedPreferences(name, mode);
    }

    public String getString(Object key) {
        return getString(key, null);
    }

    public String getString(Object key, String defaultValue) {
        try {
            return sharedPreferences.getString(key.toString(), defaultValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public Boolean getBoolean(Object key) {
        return getBoolean(key, Boolean.FALSE);
    }

    public Boolean getBoolean(Object key, Boolean defaultValue) {
        return sharedPreferences.getBoolean(key.toString(), defaultValue);
    }

    public Long getLong(Object key) {
        return getLong(key, null);
    }

    @Nullable
    public Long getLong(Object key, Long defaultValue) {
        long value = sharedPreferences.getLong(key.toString(), Long.MIN_VALUE);

        if (value != Long.MIN_VALUE) {
            return value;
        }
        return defaultValue;
    }

    public Integer getInt(Object key) {
        return getInt(key, null);
    }

    public Integer getInt(Object key, Integer defaultValue) {
        int value = sharedPreferences.getInt(key.toString(), Integer.MIN_VALUE);

		if (value != Integer.MIN_VALUE) {
			return value;
		}
		return defaultValue;
	}

	public Float getFloat(Object key) {
		return getFloat(key, null);
	}

	public Float getFloat(Object key, Float defaultValue) {
		float value = sharedPreferences.getFloat(key.toString(), Float.MIN_VALUE);

		if (value != Float.MIN_VALUE) {
			return value;
		}
		return defaultValue;
	}

	public Double getDouble(Object key) {
		return getDouble(key, null);
	}

	public Double getDouble(Object key, Double defaultValue) {
		try {
			String value = getString(key, null);

			if (value == null) {
				return defaultValue;
			}
			return Double.valueOf(value);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public void incrementInt(Object key) {
		incrementInt(key, 0);
	}

	public void incrementInt(Object key, Integer initialValue) {
		putInt(key, getInt(key, initialValue) + 1);
	}

	public void incrementLong(Object key) {
		incrementLong(key, 0L);
	}

	public void incrementLong(Object key, Long initialValue) {
		putLong(key, getLong(key, initialValue) + 1);
	}

	public void putString(Object key, String value) {
		sharedPreferences.edit().putString(key.toString(), value).apply();
	}

	public void putBoolean(Object key, Boolean value) {
		sharedPreferences.edit().putBoolean(key.toString(), value).apply();
	}

	public void putLong(Object key, Long value) {
		sharedPreferences.edit().putLong(key.toString(), value).apply();
	}

	public void putInt(Object key, Integer value) {
		sharedPreferences.edit().putInt(key.toString(), value).apply();
	}

	public void putFloat(Object key, Float value) {
		sharedPreferences.edit().putFloat(key.toString(), value).apply();
	}

	public void putDouble(Object key, Double value) {
		putString(key, value.toString());
	}

	public boolean has(Object key) {
		return sharedPreferences.contains(key.toString());
	}

	public void remove(Object key) {
		sharedPreferences.edit().remove(key.toString()).apply();
	}

	public void removeAll() {
		sharedPreferences.edit().clear().apply();
	}

	public <T> void putList(Object key, List<T> list) {
		sharedPreferences.edit().putString(key.toString(), Constants.gson.toJson(list)).apply();
	}

	public <T> List<T> getList(Object key) {
		List<T> arrayItems = new ArrayList<>();
		String serializedObject = sharedPreferences.getString(key.toString(), null);

		if (serializedObject != null) {
			Type type = new com.google.gson.reflect.TypeToken<List<T>>() {
			}.getType();
			arrayItems = Constants.gson.fromJson(serializedObject, type);
		}
		return arrayItems;
	}

}
