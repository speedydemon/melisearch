package com.lk.melisearch.activities.productSearch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.lk.melisearch.R;
import com.lk.melisearch.activities.AbstractRecyclerViewAdapter;
import com.lk.melisearch.activities.productSearch.adapters.viewHolders.ProductSearchViewHolder;
import com.lk.melisearch.activities.productSearch.fragments.ProductSearchFragment;
import com.lk.melisearch.activities.productSearch.models.Product;

import java.util.List;

import androidx.annotation.NonNull;

public class ProductSearchAdapter extends AbstractRecyclerViewAdapter<Product, ProductSearchViewHolder> {

    private final ProductSearchFragment.ProductSearchFragmentListener mListener;

    public ProductSearchAdapter(Context context, List<Product> productList, ProductSearchFragment.ProductSearchFragmentListener listener) {
        super(context, productList);
        mListener = listener;
    }

    @NonNull
    @Override
    public ProductSearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductSearchViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_product, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductSearchViewHolder holder, int position) {
        holder.bind(getItem(position));
        if (!holder.itemView.hasOnClickListeners()) {
            holder.itemView.setOnClickListener(v -> mListener.onProductDetail(getItem(position)));
        }
    }

}
