package com.lk.melisearch.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.util.AttributeSet;
import android.view.View;

import com.lk.melisearch.R;

import java.io.InputStream;

import androidx.annotation.RawRes;

public class GifView extends View {

    private Movie mMovie;
    private long lngMovie;
    private int intId = 0;

    private long lngWidth = 0;
    private long lngHeight = 0;
    private long lngDuration = 0;

    private float ratioWidth;
    private float ratioHeight;

    public GifView(Context context) {
        this(context, null);
    }

    public GifView(Context context, AttributeSet attrSet) {
        this(context, attrSet, 0);
    }

    public GifView(Context context, AttributeSet attrSet, int newStyle) {
        super(context, attrSet, newStyle);
        init(context, attrSet);
    }

    protected void init(Context context, AttributeSet attrSet) {
        if (attrSet == null) {
            return;
        }

        TypedArray array = context.obtainStyledAttributes(attrSet, R.styleable.GifView);

        try {
            this.setGifFromResource(array.getResourceId(R.styleable.GifView_src, R.drawable.loader));
        }
        finally {
            array.recycle();
        }
    }

    public void setGifFromResource(@RawRes int resourceId) {
        this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        InputStream inputStream = getContext().getResources().openRawResource(resourceId);
        mMovie = Movie.decodeStream(inputStream);

        this.intId = resourceId;
        this.lngWidth = mMovie.width();
        this.lngHeight = mMovie.height();
        this.lngDuration = mMovie.duration();
    }

    public int getGifFromResource() {
        return intId;
    }

    public long getGifWidth() {
        return lngWidth;
    }

    public long getGifHeight() {
        return lngHeight;
    }

    public long getGifDuration() {
        return lngDuration;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mMovie == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
        int width = mMovie.width();
        int height = mMovie.height();

        if (width <= 0) {
            width = 1;
        }
        if (height <= 0) {
            height = 1;
        }

        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();

        width += paddingLeft + paddingRight;
        height += paddingTop + paddingBottom;

        width = Math.max(width, getSuggestedMinimumWidth());
        height = Math.max(height, getSuggestedMinimumHeight());

        int widthSize = resolveSizeAndState(width, widthMeasureSpec, 0);
        int heightSize = resolveSizeAndState(height, heightMeasureSpec, 0);

        ratioWidth = (float) widthSize / width;
        ratioHeight = (float) heightSize / height;

        setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mMovie == null) {
            return;
        }

        long lngCurrentTime = android.os.SystemClock.uptimeMillis();

        if (lngMovie == 0) {
            lngMovie = lngCurrentTime;
        }

        int rTime = (int) ((lngCurrentTime - lngMovie) % mMovie.duration());
        mMovie.setTime(rTime);
        canvas.scale(Math.min(ratioWidth, ratioHeight), Math.min(ratioWidth, ratioHeight));
        mMovie.draw(canvas, 0, 0);
        this.invalidate();
    }
}
