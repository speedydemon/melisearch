package com.lk.melisearch.activities.productDetail.adapters;

import android.os.Bundle;

import com.lk.melisearch.activities.productDetail.fragments.ProductDetailImagePagerTabFragment;
import com.lk.melisearch.activities.productDetail.models.ProductImage;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ProductDetailImagePagerAdapter extends FragmentStateAdapter {

    public static String TAB_IMAGE = "TAB_IMAGE";

    private final List<ProductImage> mPictures;

    public ProductDetailImagePagerAdapter(@NonNull FragmentActivity activity, List<ProductImage> pictures) {
        super(activity);

        mPictures = pictures;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment = new ProductDetailImagePagerTabFragment();
        Bundle args = new Bundle();

        args.putString(TAB_IMAGE, mPictures.get(position).getSecureUrl());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getItemCount() {
        return mPictures.size();
    }

}
