package com.lk.melisearch.activities.productSearch.services;

import android.util.Log;

import com.lk.melisearch.BuildConfig;
import com.lk.melisearch.activities.AbstractService;
import com.lk.melisearch.activities.productSearch.callbacks.DataCallback;
import com.lk.melisearch.activities.productSearch.models.Product;
import com.lk.melisearch.activities.productSearch.models.ProductSearchQuery;

import java.net.HttpURLConnection;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductService extends AbstractService<ProductApi> {

    private static ProductService instance;

    private ProductService() {
        super(BuildConfig.API_URL);
    }

    public static ProductService getInstance() {
        if (instance == null) {
            instance = new ProductService();
        }
        return instance;
    }

    @Override
    protected Class<ProductApi> getServiceClass() {
        return ProductApi.class;
    }

    public void searchProducts(String searchText, DataCallback<List<Product>> callback) {
        service.searchProducts(searchText).enqueue(new Callback<ProductSearchQuery>() {
            @Override
            public void onResponse(Call<ProductSearchQuery> call, Response<ProductSearchQuery> response) {
                try {
                    if (!response.isSuccessful()) {
                        callback.onFailure(response.code());
                        return;
                    }
                    if (response.body().products.isEmpty()) {
                        callback.onFailure(HttpURLConnection.HTTP_NOT_FOUND);
                    }
                    callback.onSuccess(response.body().products);
                }
                catch (IllegalStateException exception) {
                    Log.d(TAG, TAG + " :: response ignored: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<ProductSearchQuery> call, Throwable t) {
                try {
                    Log.e(TAG, "Error on recover password", t);
                    callback.onFailure(HttpURLConnection.HTTP_GATEWAY_TIMEOUT);
                }
                catch (IllegalStateException exception) {
                    Log.d(TAG, TAG + " :: response ignored: " + t.getMessage());
                }
            }
        });
    }

    public void searchProduct(String productId, DataCallback<Product> callback) {
        service.searchProduct(productId).enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                try {
                    if (!response.isSuccessful()) {
                        callback.onFailure(response.code());
                        return;
                    }
                    callback.onSuccess(response.body());
                }
                catch (IllegalStateException exception) {
                    Log.d(TAG, TAG + " :: response ignored: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                try {
                    Log.e(TAG, "Error on recover password", t);
                    callback.onFailure(HttpURLConnection.HTTP_GATEWAY_TIMEOUT);
                }
                catch (IllegalStateException exception) {
                    Log.d(TAG, TAG + " :: response ignored: " + t.getMessage());
                }
            }
        });
    }

}
