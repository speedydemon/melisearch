package com.lk.melisearch.activities.productDetail.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lk.melisearch.R;
import com.lk.melisearch.activities.AbstractFragment;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import static com.lk.melisearch.activities.productDetail.adapters.ProductDetailImagePagerAdapter.TAB_IMAGE;

public class ProductDetailImagePagerTabFragment extends AbstractFragment {

	private AppCompatImageView mProductImage;

	/*
	 ***************** Lifecycle *****************
	 */

	@Override
	protected int getLayout() {
		return R.layout.fragment_product_detail_tab;
	}

	@NonNull
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);

		mProductImage = view.findViewById(R.id.product_image);

		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Bundle args = requireArguments();
		String pictureUrl = args.getString(TAB_IMAGE);
		Picasso.get().load(pictureUrl).into(mProductImage);
	}

}
