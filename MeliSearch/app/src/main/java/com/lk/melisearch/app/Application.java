package com.lk.melisearch.app;

import androidx.multidex.MultiDexApplication;

public class Application extends MultiDexApplication {

    private static Application instance;

    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static Application getInstance() {
        return instance;
    }

}
