package com.lk.melisearch.activities;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class AbstractRecyclerViewAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected final transient String TAG = getClass().getSimpleName();

    public static final int TYPE_LIST = 0;

    protected final Context mContext;
    protected List<T> mOriginal;
    protected List<T> mValues;
    protected boolean mUnique = true;

    public AbstractRecyclerViewAdapter(@NonNull Context context, @NonNull List<T> values) {
        this.mContext = context;
        this.mOriginal = values;
        this.mValues = values;
    }

    public int getItemViewType(int position) {
        return TYPE_LIST;
    }

    public List<T> getItems() {
        return mOriginal;
    }

    public List<T> getItemsFiltered() {
        return mOriginal;
    }

    public int getItemCount() {
        return mValues.size();
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public T getItem(int position) {
        if (position < 0 || position >= mValues.size()) {
            return null;
        }
        return mValues.get(position);
    }

    public int getItemPosition(T value) {
        return mValues.indexOf(value);
    }

    public void appendAllItems(List<T> values) {
        for (T value : values) {
            appendItem(value);
        }
    }

    public void appendAllItemsToOriginal(List<T> values) {
        for (T value : values) {
            appendItemOriginal(value);
        }
    }

    public void appendItem(T value) {
        if (isUnique() && mValues.contains(value)) {
            return;
        }

        int position = getItemCount();

        mValues.add(position, value);
        notifyItemInserted(position);
    }

    public void appendItemOriginal(T value) {
        if (isUnique() && mValues.contains(value)) {
            return;
        }

        int position = getItemCount();

        mOriginal.add(position, value);
        notifyItemInserted(position);
    }

    public void addAllItems(List<T> values) {
        for (T value : values) {
            addItem(value, 0);
        }
    }

    public void addItem(T value, int position) {
        if (isUnique() && mValues.contains(value)) {
            return;
        }

        mValues.add(position, value);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mValues.remove(position);
        notifyItemRemoved(position);
    }

    public void removeItem(T value) {
        int position = getItemPosition(value);

        if (position >= 0) {
            mValues.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        boolean needNotify = false;
        if (mOriginal != null && !mOriginal.isEmpty()) {
            needNotify = true;
            mOriginal.clear();
        }
        if (mValues != null && !mValues.isEmpty()) {
            needNotify = true;
            mValues.clear();
        }
        if (needNotify) notifyDataSetChanged();
    }

    public boolean testItem(T item, String term) {
        return item.toString().contains(term);
    }

    public void filter(String term) {
        if (term == null || term.trim().isEmpty()) {
            mValues = new ArrayList<>(mOriginal);
        } else {
            mValues = new ArrayList<>();

            for (T item : mOriginal) {
                if (testItem(item, term)) {
                    appendItem(item);
                } else {
                    removeItem(item);
                }
            }
        }
        notifyDataSetChanged();
    }

    public boolean isUnique() {
        return mUnique;
    }

    public void setUnique(boolean mUnique) {
        this.mUnique = mUnique;
    }

}
