package com.lk.melisearch.utils.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.lk.melisearch.utils.DateUtil;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateApiDeserializer implements JsonDeserializer<Date> {

    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (json.isJsonNull()) {
            return null;
        }

        String val = json.getAsString();
        Date value = DateUtil.parse(val, DateUtil.FORMAT_API);

        if (value != null) {
            // Fixed server time offset
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(value);
            calendar.add(Calendar.MILLISECOND, TimeZone.getDefault().getRawOffset());
            value = calendar.getTime();
        }
        return value;
    }

}
