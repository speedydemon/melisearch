package com.lk.melisearch.activities.productSearch;

import android.content.Intent;
import android.os.Bundle;

import com.lk.melisearch.R;
import com.lk.melisearch.activities.AbstractFragmentActivity;
import com.lk.melisearch.activities.productDetail.ProductDetailActivity;
import com.lk.melisearch.activities.productSearch.fragments.ProductSearchFragment;
import com.lk.melisearch.activities.productSearch.models.Product;

public class ProductSearchActivity extends AbstractFragmentActivity implements
    ProductSearchFragment.ProductSearchFragmentListener {

    public static String INTENT_PRODUCT_KEY = "INTENT_PRODUCT_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        replaceFragment(new ProductSearchFragment());
    }

    @Override
    public void onProductDetail(Product product) {
        Intent intent = new Intent(this, ProductDetailActivity.class);
        intent.putExtra(INTENT_PRODUCT_KEY, product.getId());
        startActivity(intent);
    }

}