package com.lk.melisearch.activities.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.lk.melisearch.activities.productSearch.ProductSearchActivity;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    /*
     ***************** Lifecycle *****************
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Handler().postDelayed(this::startMain, 1000);
    }

    private void startMain() {
        startActivity(new Intent(this, ProductSearchActivity.class));
        finish();
    }

}
