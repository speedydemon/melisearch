package com.lk.melisearch.utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    private final static String TAG = "DateUtil";

    public final static String FORMAT_API = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public final static String FORMAT_TIME = "HH:mm";

    public final static Locale defaultLocale = new Locale("es", "ES");

    public static String format(Date date) {
        return format(date, FORMAT_API);
    }

    public static String format(Date date, String pattern) {
        return format(date, pattern, defaultLocale);
    }

    public static String format(Date date, String pattern, Locale locale) {
        DateFormat formatter = new SimpleDateFormat(pattern, locale);

        formatter.setLenient(false);
        return formatter.format(date);
    }

    public static String formatTime(Date date) {
        return format(date, FORMAT_TIME);
    }

    public static String format(String date, String patternFrom, String patternTo) {
        return format(date, patternFrom, patternTo, defaultLocale);
    }

    public static String format(String date, String patternFrom, String patternTo, Locale locale) {
        return format(parse(date, patternFrom, locale), patternTo, locale);
    }

    public static Date parse(String date, String pattern) {
        return parse(date, pattern, defaultLocale);
    }

    public static Date parse(String date, String pattern, Locale locale) {
        return parse(date, pattern, defaultLocale, true);
    }

    public static Date parse(String date, String pattern, Locale locale, boolean silent) {
        try {
            DateFormat formatter = new SimpleDateFormat(pattern, locale);

            formatter.setLenient(false);
            return formatter.parse(date);
        }
        catch (ParseException e) {
            if (!silent) Log.w(TAG, "Error on parse date: " + date + " with pattern: " + pattern);
            return null;
        }
    }

    public static Calendar asCalendar(String date, String... patterns) {
        for (String pattern : patterns) {
            try {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(parse(date, pattern));
                return calendar;
            }
            catch (Exception e) {
                Log.w(TAG, "Error on as calendar date: " + date, e);
            }
        }
        return null;
    }

}
