package com.lk.melisearch.app;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lk.melisearch.utils.gson.DateApiDeserializer;
import com.lk.melisearch.utils.gson.DateDeserializer;
import com.lk.melisearch.utils.gson.DateSerializer;

import java.util.Date;

public class Constants {

	public interface EnvType {
		String PROD = "production";
		String STAG = "staging";
		String TEST = "testing";
	}

	public final static Gson gson = new GsonBuilder()
			.registerTypeAdapter(Date.class, new DateSerializer())
			.registerTypeAdapter(Date.class, new DateDeserializer())
			// .setPrettyPrinting()
			.create();

	public final static Gson gsonApi = new GsonBuilder()
			.registerTypeAdapter(Date.class, new DateSerializer())
			.registerTypeAdapter(Date.class, new DateApiDeserializer())
			// .setPrettyPrinting()
			.create();

	public final static Long APITimeout = 60L;

}
